import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

@SuppressWarnings("serial")
class GameSurface extends JPanel implements MouseListener, MouseMotionListener {

    Color aliveColor = Color.green;

    final int gridSize = 75;
    private DeathGrid grid;

    Point mouseOverCell;

    public GameSurface() {
        grid = new DeathGrid(gridSize);
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    private void doDrawing (Graphics g) {
        Graphics2D g2d = (Graphics2D) g;        

        GameSurfaceCalculator calc = new GameSurfaceCalculator();

        for (int i = 0; i < gridSize; i++) {
            for (int j = 0; j < gridSize; j++) {
                int x = j * calc.getCellWidth() + calc.getXOffset();
                int y = i * calc.getCellHeight() + calc.getYOffset();

                g2d.setColor(Color.white);
                
                this.fillCell(g2d, x, y, calc.getCellWidth(), calc.getCellHeight());

                if (grid.isAlive(j, i)) {
                    g2d.setColor(aliveColor);
                    this.fillCell(g2d, x, y, calc.getCellWidth(), calc.getCellHeight());
                }

                g2d.setColor(Color.black);
                g2d.drawRect(x, y, calc.getCellWidth(), calc.getCellHeight());
            }
        }
        
        if (mouseOverCell != null) {
            g2d.setColor(Color.cyan);
            int x = mouseOverCell.x * calc.getCellWidth() + calc.getXOffset();
            int y = mouseOverCell.y * calc.getCellHeight() + calc.getYOffset();
            
            this.fillCell(g2d, x, y, calc.getCellWidth(), calc.getCellHeight());
        }
    }

    private void fillCell(Graphics2D g2d, int x, int y, int width, int height) {
        g2d.fillRect(x + 1, y + 1, width - 1, height - 1);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        doDrawing(g);
    }
    
    public void nextStep() {
        grid.nextStep();
        triggerRedraw();
    }
    
    public void triggerRedraw() {
        validate();
        repaint();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        Point location = e.getPoint();
        
        GameSurfaceCalculator calc = new GameSurfaceCalculator();

        Point cellIndexes = calc.getCellIndexes(location);
        if (cellIndexes != null) {
            grid.toggleLife(cellIndexes.x, cellIndexes.y);
            triggerRedraw();
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        aliveColor = Color.red;
        triggerRedraw();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        aliveColor = Color.green;
        mouseOverCell = null;
        triggerRedraw();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }
    
    private class GameSurfaceCalculator {
        private int cellW;
        private int cellH;
        
        private int xOffset;
        private int yOffset;

        private int playW;
        private int playH;

        public GameSurfaceCalculator() {
            calculate();
        }
        
        private void calculate() {
            Dimension size = getSize();
            Insets insets = getInsets();
            
            int w = size.width - insets.left - insets.right;
            int h = size.height - insets.top - insets.bottom;

            cellW = w / gridSize;
            cellH = h / gridSize;

            int extraW = w % gridSize;
            int extraH = h % gridSize;

            playW = w - extraW;
            playH = h - extraH;

            xOffset = extraW / 2;
            yOffset = extraH / 2;
        }
        
        public int getCellWidth() {
            return cellW;
        }
        public int getCellHeight() {
            return cellH;
        }
        public int getXOffset() {
            return xOffset;
        }
        public int getYOffset() {
            return yOffset;
        }
        public int getPlayWidth() {
            return playW;
        }
        public int getPlayHeight() {
            return playH;
        }
        
        public Point getCellIndexes(Point location) {
            int x = location.x - xOffset;
            int y = location.y - yOffset;
            
            if (x < playW && x > 0 && y < playH && y > 0) {
                return new Point(x / cellW, y / cellH);
            }
            return null;
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        GameSurfaceCalculator calc = new GameSurfaceCalculator();
        Point tmp = calc.getCellIndexes(e.getPoint());
        if (mouseOverCell == null || tmp == null || tmp.x != mouseOverCell.x || tmp.y != mouseOverCell.y) {
            mouseOverCell = tmp;
            triggerRedraw();
        }
    }
}
