import java.util.ArrayList;


class DeathGrid {
    //static private final String rule = "012345678/3";
    //static private final String rule = "/1";
    static private final String rule = "23/3"; // GAME OF LIFE
    //static private final String rule = "1357/1357";
    
    private ArrayList<Integer> ifAliveRule = new ArrayList<Integer>();
    private ArrayList<Integer> ifDeadRule  = new ArrayList<Integer>();

    private int size;

    private boolean [][] deathGrid;

    public int getSize() {
        return size;
    }
    
    public DeathGrid(int size) {
        this.size = size > 0 ? size : 1;

        deathGrid = initDeathArray();

        // add some default life
        deathGrid[2][9]  = true;
        deathGrid[2][10] = true;
        deathGrid[2][11] = true;

        deathGrid[5][5] = true;
        deathGrid[5][6] = true;
        deathGrid[6][5] = true;
        deathGrid[6][6] = true;
        
        deathGrid[10][3] = true;
        deathGrid[11][3] = true;
        deathGrid[11][1] = true;
        deathGrid[12][3] = true;
        deathGrid[12][2] = true;

        parseRule();
    }

    private void parseRule() {
        ArrayList<Integer> currentList = ifAliveRule;

        for (int i = 0; i < rule.length(); i++) {
            char c = rule.charAt(i);
            if (c == '/') {
                if (currentList == ifAliveRule) {
                    currentList = ifDeadRule;
                } else {
                    badRule();
                }
            } else {
                c -= 48;
                if (c >= 0 && c <= 8) {
                    currentList.add((int)c);
                } else {
                    badRule();
                }
            }
        }
    }

    private void badRule() {
        System.err.println("Bad rule syntax!");
        System.exit(1);
    }

    private boolean [][] initDeathArray() {
        boolean [][] grid = new boolean[size + 2][];
        
        for (int i = 0; i < size + 2; i++) {
            grid[i] = new boolean[size + 2];
        }
        return grid;
    }

    public void nextStep() {
        boolean [][] newGrid = initDeathArray();

        for (int i = 1; i < size + 1; i++) {
            for (int j = 1; j < size + 1; j++) {
                int aliveNeighbours = 0;
                
                for (int k = i - 1; k < i + 2; k++) {
                    for (int l = j - 1; l < j + 2; l++) {
                        if ((k != i || l != j) && deathGrid[k][l]) {
                            aliveNeighbours++;
                        }
                    }
                }

                if (deathGrid[i][j]) {
                    newGrid[i][j] = ifAliveRule.contains(aliveNeighbours);
                } else {
                    newGrid[i][j] = ifDeadRule.contains(aliveNeighbours);
                }
            }
        }
        deathGrid = newGrid;
    }

    public boolean isAlive(int x, int y) {
        return deathGrid[y + 1][x + 1];
    }
    
    public void toggleLife(int x, int y) {
        deathGrid[y + 1][x + 1] = !deathGrid[y + 1][x + 1];
    }
}
