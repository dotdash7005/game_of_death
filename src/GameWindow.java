import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

class GameWindow implements ActionListener {
    private GameSurface surface;
    private Button startBtn;
    private Button stopBtn;
    
    private Timer timer;

    /**
     * @param args
     */
    public void createWindow() {
        JFrame frame = new JFrame("Game");
        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        surface = new GameSurface();
        surface.setPreferredSize(new Dimension(500, 500));

        frame.getContentPane().add(surface, BorderLayout.CENTER);

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.Y_AXIS));

        startBtn = new Button("Start");
        stopBtn  = new Button("Stop");
        stopBtn.setEnabled(false);

        buttonsPanel.add(startBtn);
        buttonsPanel.add(stopBtn);
        
        frame.getContentPane().add(buttonsPanel, BorderLayout.EAST);
        
        frame.pack();
        frame.setVisible(true);
        
        stopBtn.addActionListener(this);
        startBtn.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        if (arg0.getSource() == startBtn) {
            startBtn.setEnabled(false);
            stopBtn.setEnabled(true);

            timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    surface.nextStep();
                }
                
            }, 0, 250);
        }
        
        if (arg0.getSource() == stopBtn) {
            stopBtn.setEnabled(false);
            startBtn.setEnabled(true);

            timer.cancel();
        }
    }
}
